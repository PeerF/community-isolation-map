require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
});

module.exports = {
  siteMetadata: {
    author: 'Peer Füglistaller',
    title: 'helfen.sg',
    description:
      'Die Plattform für ein solidarisches Miteinander',
    email: 'peer@helfen.sg',
    teaserImage: "https://www.helfen.sg/fbteaser.jpg",
    twitter: {
      hashtag: 'st.gallenhilft',
      handle: 'yourTwitterHandle'

    },
    share: {
      text: 'helfen.sg | Die Plattform für ein solidarisches Miteinander',
      hashtags: 'st.gallen,helfen,solidarität,bleibtdochzuhause,unterstützen,sharingiscaring' // separate with commas
    },
    menuLinks: [
      {title: 'Start', link: '/', icon: 'map'},
      {title: 'Ich will helfen', link: '/beitragen', icon: 'plus'},
      {title: 'Kontakt', link: '/kontakt', icon: 'info'},
    ],
    mapData: {
      bounds: [
        [9.30490493774414,47.45084411487668],
        [9.44669723510742,47.39753581436993]
      ]
    },
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        // The property ID; the tracking code won't be generated without it
        trackingId: "UA-161682790-1",
      },
    },
    {
      resolve: "gatsby-plugin-firebase",
      options: {
        credentials: {
          databaseURL: process.env.FIREBASE_URL
        }
      }
    },
    {
      resolve: `gatsby-source-firebase`,
      options: {
        // point to the firebase private key downloaded
        // credential: require('./secret/firebase-creds'),

        credential: {
          "type": process.env.FIREBASE_TYPE,
          "project_id": process.env.FIREBASE_PROJECT_ID,
          "private_key": process.env.FIREBASE_PRIVATE_KEY.replace(/\\n/g, '\n'),
          "client_email": process.env.FIREBASE_CLIENT_EMAIL,
          "client_id": process.env.FIREBASE_CLIENT_ID,
          "auth_uri": process.env.FIREBASE_AUTH_URI,
          "token_uri": process.env.FIREBASE_TOKEN_URI,
          "auth_provider_x509_cert_url": process.env.AUTH_PROVIDER_X509_CERT_URL,
          "client_x509_cert_url": process.env.CLIENT_X509_CERT_URL
        },

        // your firebase database root url
        databaseURL: process.env.FIREBASE_URL,

        // you can have multiple "types" that point to different paths
        types: [
          // if you're data is really simple, this should be fine too
          {
            type: "MapPoints",
            path: "data/",
          }
        ]
      }
    }
  ],
};
