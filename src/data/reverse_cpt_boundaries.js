import * as turf from '@turf/turf'

const polygon = turf.polygon(
  [
            [
              [
                9.30490493774414,
                47.39753581436993
              ],
              [
                9.44669723510742,
                47.39753581436993
              ],
              [
                9.44669723510742,
                47.45084411487668
              ],
              [
                9.30490493774414,
                47.45084411487668
              ],
              [
                9.30490493774414,
                47.39753581436993
              ]
            ]
          ]
)

const masked = turf.mask(polygon);


export default masked;
