import React from 'react'
import { useStaticQuery, graphql } from 'gatsby';
import Layout from '../components/layout';
import { Link } from 'gatsby'
import { MapViewComponent } from '../components/mapview.main'
/* SEO Component with React Helmet */
import Head from '../components/head'

const Index = () => {

  const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          title,
          email
          teaserImage
        }
      }
    }
  `);

  return (
    <Layout>
      <Head title='Start' />
      <div className='index'>
        <section className='ui vertical very fitted segment' style={{marginTop: '1rem'}}>
          <div className='ui container'>
            <h1 className='ui header'>
              <div className='content'>
                <span className={'page-title'}>St.Gallen hilft</span>
                <div className='sub header'>
                  Der Corona-Virus betrifft uns alle. Unterstütze die Gemeinschaft und hilf deinen Mitmenschen, insbesondere denen, die in dieser schwierigen Zeit am meisten betroffen sind.{' '}
                </div>
                <div className='sub header'>
                  Beim Einkaufen gleich noch eine Tomatensauce für jemanden aus dem Quartier mitnehmen? Die Medikamente von der Oma nebenan abholen? Mit dem Hund vom Nachbarn Gassi gehen, der in Selbstisolation bleiben sollte?{' '}
                </div>
                <div className='sub header'>
                  Hier kannst du etwas zum Wohlergehen in St. Gallen beitragen. Bewirke mit kleinen Gesten Grosses <i className={'ui icon heart'} />
                </div>
                <div className='sub header'>
                  {' '}
                  <Link to={'/beitragen'}>
                    Trage dich hier ein,
                  </Link>
                  {' '}um zu helfen.
                </div>
              </div>
            </h1>
          </div>
        </section>
        <section>
          <MapViewComponent />
        </section>
        <section className='ui vertical segment intro'>
          <div className='ui text container formcontainer'>
          <div className={'ui yellow message'} style={{fontWeight: 500, fontStyle: 'italic'}}>
            <i className={'icon external link'} />Erfahre <a href={'https://medium.com/@marcfehr/how-to-build-a-fast-and-reliable-community-mapping-tool-with-gatsbyjs-and-firebase-74a0fa6b5b83?source=userActivityShare-f57d26da4972-1584988662&_branch_match_id=689400773593121406'} target={'_blank'} rel={'noopener noreferrer'}>hier</a> wie Du ein eigenes Community Mapping Tool erstellen kannst.
          </div>
            <h2>Was sehe ich auf dieser Karte?</h2>
            <p>
              Im Moment zeigt diese Karte Menschen, die Unterstützung anbieten im Raum St. Gallen. Einige Neuerungen sind in Planung:
            </p>
            <ul>
              <li>Eine <strong>"Hilfe"-Funktion für Menschen die diese benötigen</strong> und deren Daten beschützt werden sollen.</li>
              <li>Eine Option für <strong>Dienstleistungen die nicht an einen Standort gebunden sind</strong></li>
              <li>Die Möglichkeit, <strong>mehrere Kategorien </strong>auszuwählen</li>
            </ul>
            <Link
              to={'/beitragen'}
              className='ui primary fluid button'
              style={{marginTop: '1rem'}}
            >
              Jetzt mitmachen
            </Link>
            <h2>Wie kann ich meinen Eintrag von der Karte löschen?</h2>
            <p>
              Schreib mir ein Mail an{' '}
              <a
                href={`mailto:${data.site.siteMetadata.email}`}
                target='_blank'
                rel='noopener noreferrer'
              >
                {data.site.siteMetadata.email}
              </a>
              .{' '}Ansonsten werden deine Daten automatisch gelöscht, sobald dieser Service nicht mehr benötigt wird.
            </p>
          </div>
        </section>
      </div>
    </Layout>
  );
};

export default Index;
