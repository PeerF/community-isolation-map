import React from 'react';
import { Link } from 'gatsby'
/* SEO Component with React Helmet */
import Head from '../components/head'
import Layout from '../components/layout'
import { MapAddComponent } from '../components/mapview.add'

const Add = () => {
  return (
    <Layout>
      <Head title={`Beitragen`} />
      <div className='index'>
        <section className='ui vertical very fitted segment' style={{marginTop: '1rem'}}>
          <div className='ui container'>
            <h1 className='ui header'>
              <div className='content'>
              <span className='page-title'>
                Unterstütze deine Gemeinde
              </span>
                <div className='sub header'>
                  Seid vorsichtig, helft wo ihr könnt und behaltet einen kühlen Kopf. Vielen Dank für eure Beiträge auf der Karte unten.
                </div>
              </div>
            </h1>
          </div>
        </section>

        <section className='ui vertical segment'>
          <div className='ui text container formcontainer'>
            <h2>So funktioniert's</h2>
            Wähle einen Ort auf der Karte, an dem deine Dienstleistung angezeigt werden soll. Trage dann ein paar Infos ein und sende das Formular ab. <strong>Alle Einträge werden manuell freigeschaltet</strong>. Sobald wir dein Formular überprüft haben, wirst du deinen Beitrag {' '}
            <Link to='/'>
              hier
            </Link> sehen.
          </div>
        </section>
        <section>
          <MapAddComponent />
        </section>

      </div>
    </Layout>
  );
};

export default Add;
