import React from 'react';

import Layout from '../components/layout';
import Head from '../components/head';

const Contact = () => {
  return (
    <Layout>
      <Head title={`Kontakt`} />
      <section className='ui vertical very fitted segment' style={{marginTop: '1rem'}}>
        <div className='ui container'>
          <h1 className='ui header'>
            <div className='content'>
            <span className='page-title'>
              Kontakt aufnehmen.
            </span>
              <div className='sub header'>
                Feedback ist sehr willkommen. Kontaktiere uns per Email mit deinem Anliegen.
                Dieses Projekt ist OpenSource und auf <a href={'https://gitlab.com/marc.fehr/community-isolation-map'} target={'_blank'} rel={'noopener noreferrer'}>Gitlab</a> verfügbar.
              </div>
            </div>
          </h1>
          <div className={'ui yellow message'} style={{fontWeight: 500, fontStyle: 'italic'}}>
          <i className={'icon external link'} />Erfahre <a href={'https://medium.com/@marcfehr/how-to-build-a-fast-and-reliable-community-mapping-tool-with-gatsbyjs-and-firebase-74a0fa6b5b83?source=userActivityShare-f57d26da4972-1584988662&_branch_match_id=689400773593121406'} target={'_blank'} rel={'noopener noreferrer'}>hier</a> wie Du ein eigenes Community Mapping Tool erstellen kannst.
          </div>
        </div>
      </section>

      <section className='ui vertical segment' style={{minHeight: '55vh'}}>
        <div className='ui text container formcontainer'>
          <h2>Kontaktaufnahme</h2>
          <ul>
            <li>
            <a href='mailto:peer@helfen.sg'>
              Peer Füglistaller
            </a>, Entwickler und Betreiber von{' '}
            <a href={'https://www.helfen.sg'} target={'_blank'} rel={'noopener noreferrer'}>
              helfen.sg
            </a>
            </li>
            <li>
              {/* If you're a developer, add yourself here and create a MR on Gitlab do get onto the original repository */}
              <a href='mailto:marc.fehr@gmail.com'>
                Marc Fehr
              </a>, Entwickler und Betreiber von{' '}
              <a href={'https://www.whozinberg.org'} target={'_blank'} rel={'noopener noreferrer'}>
                whozinberg.org
              </a>
            </li>
          </ul>
          <h2>Projekte</h2>
          <p>Sieh dir diese tollen Projekte an:</p>
          <ul>
            <li>
            <a href={'https://www.facebook.com/groups/189777652351193'} target={'_blank'} rel={'noopener noreferrer'}>
              Facebook-Gruppe "Gern gescheh - St. Gallen hilft"
            </a>
            </li>
            <li>
            <a href={'https://regional-helfen.ch/'} target={'_blank'} rel={'noopener noreferrer'}>
              Regional helfen
            </a>
            </li>
            <li>
            <a href={'https://supportyourlocalartist.ch'} target={'_blank'} rel={'noopener noreferrer'}>
              Support your local Artist
            </a>
            </li>
            <li>
            <a href={'https://www.tagblatt.ch/ostschweiz/corona-krise-ostschweiz-einkaufshilfen-hotlines-anlaufstellen-ld.1205577'} target={'_blank'} rel={'noopener noreferrer'}>
              Übersicht der Angebote, Tagblatt St. Gallen
            </a>
            </li>
          </ul>
          <h2>Vernetzen</h2>
          <p>Unterstütze uns und teile diese Seite auf den Social Media Kanälen deines Vertrauens. Vielen Dank.</p>
        </div>
      </section>
    </Layout>
  );
};

export default Contact;
