module.exports = [
{ ident: 'einkaufen', text: 'Einkaufen', color: '#1289A7' },
{ ident: 'jemandenzumreden', text: 'Jemanden zum reden', color: '#7f8c8d' },
{ ident: 'wohnraum', text: 'Wohnraum', color: '#D980FA' },
{ ident: 'medizinischeunterstuetzung', text: 'Medizinische Unterstützung', color: '#30336b' },
{ ident: 'kochen', text: 'Kochen', color: '#8e44ad' },
{ ident: 'lieferdienst', text: 'Lieferdienst', color: '#2980b9' },
{ ident: 'fahrdienst', text: 'Fahrdienst', color: '#d35400' },
{ ident: 'haustierbetreuung', text: 'Haustierbetreuung', color: '#e1b12c' },
{ ident: 'anderes', text: 'Anderes', color: '#222f3e' }
]
