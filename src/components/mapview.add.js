import React, { useState } from 'react'
import './mapview.add.scss'
import AnimatedMap from './map-add/animatedmap/component.js'
import categories from '../components/categories'
import { useStaticQuery, graphql, Link } from 'gatsby';
import firebase from "gatsby-plugin-firebase";

const scrollToElement = require('scroll-to-element');

/*
See gatsby-config.js in main dir for bounds
 */

export function MapAddComponent() {

  const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          title,
          share {
            text,
            hashtags
          },
          mapData {
            bounds
          }
        },
      }
    }
  `);

  const [mapActive, setMapActive] = useState(false);
  const [map, setMap] = useState(null);
  const [positionSelected, setPositionSelected] = useState(false);
  const [formSent, setFormSent] = useState(false);
  const [showError, setShowError] = useState(false);
  const [content, setContent] = useState({
    position: [],
    category: '',
    title: '',
    description: '',
    contact: '',
    address: '',
    phone: '',
    email: '',
    name: '',
    timestamp: Date.now(),
    approved: false
  })

  const onChange = e => {
    // content[e.target.name] = e.target.value
    const c = { ...content }
    c[e.target.name] = e.target.value
    setContent(c)
  };

  React.useEffect(() => {
    if (mapActive) {
      map.on('click', e => {
        const pos = [e.lngLat.lng, e.lngLat.lat]

        console.log(pos);
        setContent({ ...content, position: pos })
        map.getSource('position').setData({
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              geometry: { type: 'Point', coordinates: pos }
            }
          ]
        })
      });

      // Fit effect
      map.fitBounds(
        data.site.siteMetadata.mapData.bounds,
        { duration: 700 }
      )
    }
  }, [mapActive]);

  React.useEffect(() => {
    scrollToElement('#formcontent')
  }, [positionSelected]);

  React.useEffect(() => {
    if (formSent === true) {
      const newPostKey = firebase
        .database()
        .ref()
        .child('data')
        .push().key

      firebase
        .database()
        .ref('/data/' + newPostKey)
        .update(content)
        .then(() => {
          console.log('Writing done')
        })
        .catch(e => {
          console.log('error', e)
        })
    }
  }, [formSent])

  const validateForm = () => {
    let error = false
    error = content.title.length === 0 ? true : error
    error = content.description.length === 0 ? true : error
    error = content.address.length === 0 ? true : error
    error = content.contact.length === 0 ? true : error
    error = content.name.length === 0 ? true : error
    error = content.email.length === 0 ? true : error
    error = content.category.length === 0 ? true : error

    if (error) {
      setShowError(true)
    } else {
      setFormSent(true)
    }
  }

  return (
    <div id={'map-add-component'}>
      <div
        id='mapcontainer'
        style={{ display: positionSelected ? 'none' : 'inherit' }}
      >
        <AnimatedMap getMapObject={m => setMap(m)} enabled={mapActive} />
        {!mapActive && (
          <div id='overlay' className='box'>
            <h3>Eintrag hinzufügen</h3>
            <p>
              Wähle einen Punkt auf der Karte um Teil von helfen.sg zu werden.
            </p>
            <button
              className='ui primary button'
              onClick={() => setMapActive(true)}
            >
              Karte aktivieren und Service hinzufügen
            </button>
          </div>
        )}

        {content.position.length > 0 && (
          <div id='selectThisPoint' className='box'>
            <h3>Du hast eine Location ausgewählt</h3>
            <p>Möchtest du hier deinen Pin setzen?</p>
            <div className='ui buttons'>
              <button
                className='ui button'
                onClick={() => {
                  setContent({ ...content, position: [] })
                }}
              >
                Nein, nochmals wählen.
              </button>
              <button
                className='ui positive button'
                onClick={() => setPositionSelected(true)}
              >
                Ja!
              </button>
            </div>
          </div>
        )}
      </div>

      {positionSelected && !formSent && (
        <div id='formcontent' className='ui vertical segment'>
          <div className='ui text container formcontainer'>
            <button
              className='ui left labeled icon button compact'
              onClick={() => {
                setPositionSelected(false)
                setContent({ ...content, position: [] })
              }}
            >
              <i className='left arrow icon' />
              Standort ändern
            </button>
            <div className='ui form'>
              <h4 className='ui horizontal divider header'>
                Über deinen Service (öffentlich)
              </h4>
              <p>
                Bitte beantworte die folgende Frage. Diese Information wird öffentlich auf der Karte angezeigt. Bitte erstelle für jeden Service einen eigenen Eintrag, wenn du mehr als einen Service bieten kannst.
              </p>

              <div className='field'>
                <label>Service Kategorie</label>
                <select
                  value={content.category}
                  className='ui dropdown'
                  onChange={e =>
                    setContent({ ...content, category: e.target.value })
                  }
                >
                  <option value='' />
                  {categories.map(c => (
                    <option value={c.ident} key={c.ident}>
                      {c.text}
                    </option>
                  ))}
                </select>
                {/*
                <CategoryButtons
                  onClick={name => setContent({ ...content, category: name })}
                  selected={content.category}
                /> */}
              </div>

              <div className='field required'>
                <label>Titel deines Beitrags</label>
                <input
                  type='text'
                  name='title'
                  value={content.title}
                  onChange={onChange}
                  placeholder='Hilfe beim Einkaufen / Fahrdienst / Was auch immer?'
                />
              </div>

              <div className='field required'>
                <label>Was bietest du an</label>
                <textarea
                  rows={4}
                  name='description'
                  onChange={onChange}
                  placeholder='Schreibe eine kurze Anleitung deiner Angebote und Dienstleistungen'
                  defaultValue={content.description}
                />
              </div>

              <div className='field required'>
                <label>Kontaktaufnahme</label>
                <textarea
                  rows={4}
                  name='contact'
                  placeholder='Öffentliche Info; Wie kann man mit dir Kontakt aufnehmen?'
                  defaultValue={content.contact}
                  onChange={onChange}
                />
              </div>

              <div className='field required'>
                <label>Physische Adresse</label>
                <textarea
                  rows={4}
                  name='address'
                  placeholder='Ungefähre Adresse, z.B. Multergasse St. Gallen. Diese Information benötigen wir um deinen Standort auf der Karte zu bestätigen'
                  defaultValue={content.address}
                  onChange={onChange}
                />
              </div>

              <h4 className='ui horizontal divider header'>
                Weitere Informationen
              </h4>
              <p>
                Dieser Eintrag wird nicht auf der Website publiziert.
              </p>

              <div className='field required'>
                <label>Dein Name</label>
                <input
                  type='text'
                  name='name'
                  placeholder='Hans Meier'
                  defaultValue={content.name}
                  onChange={onChange}
                />
              </div>

              <div className='field required'>
                <label>Deine Emailadresse</label>
                <input
                  type='text'
                  name='email'
                  placeholder='hans.meier@bluewin.ch'
                  defaultValue={content.email}
                  onChange={onChange}
                />
              </div>

              <div className='field'>
                <label>Telefonnummer (nicht obligatorisch)</label>
                <input
                  type='text'
                  name='phone'
                  placeholder='079...'
                  defaultValue={content.phone}
                  onChange={onChange}
                />
              </div>

              {showError && (
                <div className='ui negative message'>
                  <div className='header'>Es fehlen noch Daten...</div>
                  <p>
                    Bitte fülle alle Pflichtfelder aus.
                  </p>
                </div>
              )}

              <div className='ui buttons'>
                <button className='ui positive button' onClick={validateForm}>
                  Absenden
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
      {formSent && (
        <div className='ui vertical segment'>
          <div className='ui text container'>
            <div className='ui success message'>
              <div className='header'>Danke!</div>
              <p>
                Deine Daten wurden erfolgreich abgesendet und werden in Kürze auf{' '}
                <Link to='/'>{' '}{data.site.siteMetadata.title}
                </Link>{' '}
                angezeigt werden.
              </p>
            </div>
          </div>
        </div>
      )}

      <div className='ui vertical segment'>
        <div className='ui text container formcontainer'>
          <h2>Was passiert mit meinen Daten?</h2>
          helfen.sg überprüft deine Angaben und veröffentlicht diese auf der interaktiven Karte. Die Prüfung sollte in <strong>weniger als 24 Stunden stattfinden</strong>.<br />
          <h2>Welche Einträge werden zugelassen?</h2>
          Jeder kann Teil der helfen.ch-Community werden.
          Egal ob du Kinderbücher via Skype vorliest, jeden Mittwoch dein berühmtes Sauerteigbrot ausliefern möchtest, oder einfach einen TakeAway-Service für deine Produkte anbieten willst - hier bist du richtig.
          <h2>Wie kann ich meine Daten und Einträge entfernen?</h2>
          Wenn du <strong>von der Karte gelöscht werden willst,</strong> schreib eine Nachricht an{' '}
          <a
            href='mailto:peer@helfen.sg'
          >
            peer@helfen.sg
          </a>
          <h2>Gibt es etwas, das wir verbessern können?</h2>
          Diese Plattform soll etwas nützen. Dazu brauchen wir dein Feedback. Wenn du eine gute Idee oder sonst etwas zu erzählen hast, melde dich via <a
          href='mailto:peer@helfen.sg'
        >
           Email
        </a>{' '}.
          <h2>Warum nur St. Gallen?</h2>
          Es macht Sinn, so lokal wie möglich zu bleiben. Wenn du diese Plattform auch in deiner Umgebung eröffnen möchtest,{' '}
          <a
          href='mailto:peer@helfen.ch'
        >
            nimm Kontakt auf
        </a>{' '}und wir reden drüber.
        </div>
      </div>
    </div>
  )
}
