import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';

import './footer.scss';

const Footer = () => {
  const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          author,
          title,
          email,
          twitter {
            hashtag
          }
        }
      }
    }
  `);

  return (
    <footer className='ui vertical inverted segment'>
      <div className='ui center aligned container'>
        <i className='ui icon copyright' />{new Date().getFullYear()}{' '}{data.site.siteMetadata.title}{' ' }|{' '}<a href={`mailto:${data.site.siteMetadata.email}`}><i className='ui icon envelope' />Kontakt aufnehmen</a>{' '} | {' '}Mit <i className={'ui icon heart'} />entwickelt von <a href='https://twitter.com/@mrcfhr' target={'_blank'} rel={'noopener noreferrer'}>Marc Fehr</a> und <a href='mailto:peer@helfen.sg' target={'_blank'} rel={'noopener noreferrer'}>Peer Füglistaller</a>.
      </div>
    </footer>
  );
};

export default Footer;
