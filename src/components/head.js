import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import { Helmet } from 'react-helmet';

const Head = ({ title }) => {
  const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          title
          description
          teaserImage
        }
      }
    }
  `);

  return (
    <Helmet
      htmlAttributes={{ lang: 'en' }}
      title={`${title} | ${data.site.siteMetadata.title}`}
      meta={[
        { name: 'description', content: data.site.siteMetadata.description },
        { name: 'og:url', content: "https://www.helfen.sg" },
        { name: 'og:image', content: "https://www.helfen.sg/fbteaser.jpg" },
        { name: 'og:image:secure_url', content: data.site.siteMetadata.teaserImage },
        { name: 'og:locale', content: "en_GB"},
        { name: 'og:image:type', content: "image/jpeg"},
        { name: 'og:image:alt', content: "helfen.sg Teaser Image." },
      ]}
    />
  );
};

export default Head;
